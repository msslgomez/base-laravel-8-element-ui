<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Inertia::version(function () {
            return md5_file(public_path('mix-manifest.json'));
        });
        Inertia::share('prop', function () {
            $auth = null;
            if (Auth::user()) {
                $auth = [
                    'id' => Auth::user()->id,
                    'name' => Auth::user()->name,
                    'email' => Auth::user()->email
                ];
            }
            return [
                'app' => [
                    'name' => Config::get('app.name'),
                ],
                'auth' => [
                    'user' => $auth,
                ],
                'flash' => [
                    'success' => Session::get('success'),
                ],
                'errors' => Session::get('errors') ? Session::get('errors')->getBag('default')->getMessages() : (object) [],
            ];
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
